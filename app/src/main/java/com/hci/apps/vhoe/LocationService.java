package com.hci.apps.vhoe;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.IBinder;
import android.widget.Toast;

import com.hci.apps.vhoe.utils.BluetoothManager;
import com.hci.apps.vhoe.utils.OrientationManager;
import com.hci.apps.vhoe.utils.SimpleLocation;
import com.hci.apps.vhoe.utils.SingleShotLocationProvider;

import static android.util.Log.d;

/**
 * Created by hesham on 19.06.17.
 */

public class LocationService extends Service implements SingleShotLocationProvider.LocationCallback {
    private static final String TAG = SimpleLocation.class.getSimpleName();
    private SimpleLocation simpleLocation;
    private OrientationManager oMgr;
    boolean updatedLocation  =false;
    final char[] dirs={'A','K','U','_','i','s'};
    public static BluetoothManager bmgr = MainActivity.bluetoothManager;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        oMgr = OrientationManager.getInstance(this);
        simpleLocation = new SimpleLocation(getApplicationContext(), true);
        if (!simpleLocation.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        } else {
            SingleShotLocationProvider.requestSingleUpdate(getApplicationContext(), this);
        }

    }

    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location){
        d(TAG, "onNewLocationAvailable: latitude: " + location.latitude + " longitude: " + location.longitude +" Orientation is "+ oMgr.getAzimut());
        SharedPreferences prefs = getSharedPreferences(ReferenceLocationActivity.MY_PREFS_NAME, MODE_PRIVATE);
        String longitude = prefs.getString("longitude", null);
        String latitude = prefs.getString("latitude", null);
        if (longitude != null && latitude!= null) {
            Location ref =  new Location("");
            ref.setLongitude(Double.parseDouble(longitude));
            ref.setLatitude(Double.parseDouble(latitude));
            Location loc = new Location("");
            loc.setLatitude(location.latitude);
            loc.setLatitude(location.longitude);
            float distance = loc.distanceTo(ref);
            float angle = oMgr.getAzimut() - (loc.bearingTo(ref));
            int strength = (int) Math.log(distance) ;
            if(strength>9)
                strength=9;
            if(angle <0)
                angle+=360;
            int idx = (int) angle*5/360;
            if(idx > 5)
                idx=5;
            if(idx < 0)
                idx=0;
            idx = 5-idx;
            char tosend='a';
            tosend = (char)(((int)dirs[idx])+strength);
            if(bmgr != null)
                bmgr.sendMessage(String.valueOf(tosend));
            Toast.makeText(this,  "onNewLocationAvailable: latitude: " + location.latitude + " longitude: " + location.longitude +" Orientation is "+ oMgr.getAzimut()+" distance: "+distance +" bearing= "+ angle, Toast.LENGTH_SHORT).show();
        }
        updatedLocation=true;
        stopSelf();
    }


    @Override
    public void onDestroy() {
        simpleLocation.endUpdates();
        oMgr.stop();
        d(TAG, "stopping service");
        if (updatedLocation) {
            // restart in 1 min
            d(TAG, "starting alarm");
            AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarm.set(
                    alarm.RTC_WAKEUP,
                    System.currentTimeMillis() + (1000 * 20),
                    PendingIntent.getService(this, 0, new Intent(this, LocationService.class), 0)
            );
        }
    }


}

package com.hci.apps.vhoe.utils;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.hci.apps.vhoe.BluetoothService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.hci.apps.vhoe.MainActivity.BLUETOOTH_REQUEST_CODE;

/**
 * Created by root on 21/05/17.
 */

public class BluetoothManager {
    private BluetoothAdapter bAdapter;
    private Activity act;
    private SingBroadcastReceiver mReceiver;
    private List<BluetoothDevice> discoveredDevices;
    private List<BluetoothDevice> pairedDevices;
    private static BluetoothService mBluetoothService;
    public static BluetoothManager helper = null;
    private static String name;
    private List<BluetoothListener> listeners= new ArrayList<BluetoothListener>();

    public static BluetoothManager getInstance(){
        return helper;
    }

    public static BluetoothManager getInstance(Activity act){
        if(helper == null){
            helper  = new BluetoothManager(act);
        }
        return helper;
    }

    public  BluetoothManager(Activity act){
        if(helper == null)
            helper = this;

        this.act= act;
        // Getting LocationManager object from System Service LOCATION_SERVICE
        if (ContextCompat.checkSelfPermission(act,
                Manifest.permission.BLUETOOTH)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(act,
                    Manifest.permission.BLUETOOTH)) {
                Toast.makeText(act,"The device needs to be discoverable via Bluetooth so that the belt can connect to it",Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(act,
                        new String[]{Manifest.permission.BLUETOOTH,Manifest.permission.BLUETOOTH_ADMIN},
                        BLUETOOTH_REQUEST_CODE);
                return;
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(act,
                        new String[]{Manifest.permission.BLUETOOTH,Manifest.permission.BLUETOOTH_ADMIN},
                        BLUETOOTH_REQUEST_CODE);
                return;
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        bAdapter = BluetoothAdapter.getDefaultAdapter();
        discoveredDevices = new ArrayList<BluetoothDevice>();

        if (bAdapter == null){
            //handle no bluetooth
        }

        if (!bAdapter.isEnabled()){
            Intent intentBluetooth = new Intent();
            intentBluetooth.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
            act.startActivity(intentBluetooth);

        }
        ensureDiscoverable();
        if(mBluetoothService == null) {
            mBluetoothService = new BluetoothService(act);
            startService();
        }
        //get paired devices & start discovery
        refreshList();
        if(mReceiver== null)
            mReceiver = new SingBroadcastReceiver();
        IntentFilter ifilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        act.registerReceiver(mReceiver, ifilter);


    }


    public  BluetoothService getService(){
        if(mBluetoothService== null){
            mBluetoothService = new BluetoothService(act);
            startService();
        }
        return mBluetoothService;
    }

    public void startService(){
        if (getService() != null) {
            if (getService().getState() == mBluetoothService.STATE_NONE) {
                // Start the Bluetooth chat services
                getService().start();
            }
        }
    }
    public void stopService(){
        if (getService() != null) {
            if (getService().getState() == mBluetoothService.STATE_CONNECTED) {
                // Start the Bluetooth chat services
                getService().stop();
            }
        }
    }
    public void sendMessage(String Message){
        if(getService() != null){
            getService().sendMessage((Message));
        }
    }

    public  void connectDevice(BluetoothDevice device, boolean secure) {
        // Attempt to connect to the device
        getService().connect(device, secure);
    }

    public void refreshList(){

        //get paired devices
        Set<BluetoothDevice> pDevices = bAdapter.getBondedDevices();
        pairedDevices = new ArrayList<BluetoothDevice>();
        for(BluetoothDevice bt : pDevices)
            pairedDevices.add(bt);


        //cancel any prior BT device discovery
        if (bAdapter.isDiscovering()){
            bAdapter.cancelDiscovery();
        }
        //re-start discovery
        bAdapter.startDiscovery();
        BluetoothListener(pairedDevices,discoveredDevices);
    }


    public List<BluetoothDevice> getDiscoveredDevices(){
        return discoveredDevices;
    }

    public List<BluetoothDevice> getPairedDevices(){
        return pairedDevices;
    }

    private void BluetoothListener(List<BluetoothDevice> pairedDevices,List<BluetoothDevice> discoveredDevices){
        for (BluetoothListener listener: listeners) {
            listener.deviceUpdated(pairedDevices,discoveredDevices);
        }
    }
    public List<BluetoothListener> getListeners() {
        return listeners;
    }

    public void addListener(BluetoothListener listeners) {
        this.listeners.add(listeners);
        getService().addListener(listeners);
    }
    public void removeListener(BluetoothListener listeners) {
        this.listeners.remove(listeners);
    }

    private class SingBroadcastReceiver extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction(); //may need to chain this to a recognizing function
            if (BluetoothDevice.ACTION_FOUND.equals(action)){
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a Toast
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    discoveredDevices.add(device);
                }
            }else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                BluetoothListener(pairedDevices, discoveredDevices);
            }
        }
    }
    private void ensureDiscoverable() {

        if (bAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            act.startActivity(discoverableIntent);
        }
    }

    public String getLocalBluetoothName(){
        if(name != null && !name.isEmpty())
            return name ;
        if(bAdapter == null){
            bAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        String n = bAdapter.getName();
        if(n == null){
            System.out.println("Name is null!");
            n = bAdapter.getAddress();
        }
        name =n;
        return name;
    }




}
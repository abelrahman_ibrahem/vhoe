package com.hci.apps.vhoe;

import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by abdo on 09/12/16.
 */

public class BluetoothDevicesAdapter extends RecyclerView.Adapter<BluetoothDevicesAdapter.ViewHolder> {
    private static final String TAG = BluetoothDevicesAdapter.class.getSimpleName();
    public List<BluetoothDevice> discoveredDevices;
    public List<BluetoothDevice> pairedDevices;
    private BluetoothDevice selectedDevice;
    int selectedindex=-1;
    public BluetoothDevicesAdapter(List<BluetoothDevice> pairedDevices, List<BluetoothDevice> discoveredDevices){
        this.discoveredDevices = discoveredDevices;
        this.pairedDevices= pairedDevices;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bluetooth_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.d(TAG,"position is "+position);
        if(position!= pairedDevices.size()) {
            final BluetoothDevice device = position < pairedDevices.size() ? pairedDevices.get(position) : discoveredDevices.get(position- pairedDevices.size()-1);
            Log.d(TAG,"device is "+device.getName());
            holder.mTextView.setText(device.getName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedDevice = device;
                    selectedindex=position;
                    holder.mcardView.setBackgroundColor(Color.BLUE);
                    holder.mTextView.setTextColor(Color.WHITE);
                    BluetoothDevicesAdapter.this.notifyDataSetChanged();
                }
            });
            if(position == selectedindex){
                holder.mcardView.setBackgroundColor(Color.BLUE);
                holder.mTextView.setTextColor(Color.WHITE);
            }else{
                holder.mcardView.setBackgroundColor(Color.TRANSPARENT);
                holder.mTextView.setTextColor(Color.BLACK);
            }
        }else{
            holder.mcardView.setMinimumHeight(1);
            holder.mcardView.setBackgroundColor(Color.GRAY);
            holder.mTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return discoveredDevices.size()+pairedDevices.size()+1;
    }
    public BluetoothDevice getSelectedDevice(){
        return selectedDevice;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public LinearLayout mcardView;
        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView)v.findViewById(R.id.info_text);
            mcardView = (LinearLayout) v.findViewById(R.id.card_view);
        }
    }

}

package com.hci.apps.vhoe.models;

import com.hci.apps.vhoe.utils.BluetoothManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abdo on 07/01/17.
 */

public class Message {
    public enum Type {
        State("state"),
        MESSAGE("Message"),
        ERROR("Error"),
        PLAY("play")
        ;

        private String d ;
        Type(String data){
            d=data;
        }

        @Override
        public String toString() {
            return d;
        }
        public static Type fromString(String text) {
            if (text != null) {
                for (Type b : Type.values()) {
                    if (text.equalsIgnoreCase(b.toString())) {
                        return b;
                    }
                }
            }
            return null;
        }
    }

    private Type type;
    private String from;
    private String to ;
    private String data;

    public Message(){

    }
    public Message(String message){
        try {
            JSONObject obj = new JSONObject(message);
            this.to = obj.getString("to");
            this.from = obj.getString("from");
            this.data = obj.getString("data");
            this.type = Type.fromString(obj.getString("type"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public Message(Type type, String from, String to, String data) {
        this.type = type;
        this.from = from;
        this.to = to;
        this.data = data;
        if(from.isEmpty() && to.isEmpty()) {
            this.from = BluetoothManager.getInstance().getLocalBluetoothName();
            this.to = BluetoothManager.getInstance().getLocalBluetoothName();
        }
    }

    @Override
    public String toString() {
        return "{" +
                "type:'" + type.toString() + '\'' +
                ", from:'" + from + '\'' +
                ", to:'" + to + '\'' +
                ", data:'" + data + '\'' +
                '}';
    }
}

package com.hci.apps.vhoe.utils;

/**
 * Created by hesham on 31.05.17.
 */

public interface OnConnectListener {

    public void onConnect();
}

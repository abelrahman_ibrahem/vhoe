package com.hci.apps.vhoe;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hci.apps.vhoe.utils.BluetoothManager;

public class DemoActivity extends Activity {

    EditText editText;
    Button button;
    public static BluetoothManager bmgr = MainActivity.bluetoothManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        editText = (EditText) findViewById(R.id.demo_text);
        button = (Button) findViewById(R.id.b_send);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bmgr != null)
                    bmgr.sendMessage(editText.getText().toString());
            }
        });
    }
}

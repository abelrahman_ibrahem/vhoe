package com.hci.apps.vhoe;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.hci.apps.vhoe.models.Message;
import com.hci.apps.vhoe.utils.BluetoothListener;
import com.hci.apps.vhoe.utils.BluetoothManager;

import java.util.List;

public class BluetoothActivity extends AppCompatActivity implements BluetoothListener, View.OnClickListener {
    private static final String TAG = BluetoothActivity.class.getSimpleName();
    private BluetoothDevicesAdapter madapter;
    private RecyclerView Rview;
    public static BluetoothManager bmgr = MainActivity.bluetoothManager;
    private FloatingActionButton fapConnect;
    public static BluetoothDevice connectedDevice= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        fapConnect= (FloatingActionButton)findViewById(R.id.fabConnect);
        fapConnect.setOnClickListener(this);
        Rview = (RecyclerView) findViewById(R.id.bluetooth_devices);
        if(bmgr == null)
            bmgr = BluetoothManager.getInstance(this);


        madapter = new BluetoothDevicesAdapter(bmgr.getPairedDevices(),bmgr.getDiscoveredDevices());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        if(bmgr.getPairedDevices().size() ==0  && bmgr.getDiscoveredDevices().size() ==0){
            Snackbar.make(fapConnect, "Sorry, No Bluetooth devices found", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        Rview.setLayoutManager(mLayoutManager);
        Rview.setAdapter(madapter);
    }

    @Override
    protected void onStart() {
        if(bmgr!= null)
            bmgr.startService();
        bmgr.addListener(this);
        super.onStart();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        return true;

    }

    @Override
    public void handleMessage(Message message) {

    }

    @Override
    protected void onStop() {
        Log.d(TAG,"removing listener");
        bmgr.removeListener(this);
        super.onStop();
    }

    @Override
    public void deviceUpdated(List<BluetoothDevice> pairedDevices, List<BluetoothDevice> discoveredDevices) {
        Log.d(TAG, "devices list updated");
        for (BluetoothDevice device : pairedDevices) {
            Log.d(TAG, "paired: " + device.getName());
        }
        for (BluetoothDevice device : discoveredDevices) {
            Log.d(TAG, "discvered: " + device.getName());
        }

        if (madapter != null) {
            madapter.pairedDevices = pairedDevices;
            madapter.discoveredDevices = discoveredDevices;
            Log.d(TAG, "count is " + madapter.getItemCount());
            madapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDeviceConnect(BluetoothDevice device) {
            Log.d(TAG,"Device connected starting activitz");
//            Intent i = new Intent(this, DemoActivity.class);
//            startActivity(i);
            connectedDevice= device;
        Toast.makeText(this,"Connected to device "+ device.getName() +" you can now play",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.fabConnect){
            if(madapter.getSelectedDevice()!= null && bmgr!=null) {
                bmgr.connectDevice(madapter.getSelectedDevice(), false);

                Snackbar.make(fapConnect, "Connecting to Device: "+madapter.getSelectedDevice().getName() , Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

}

package com.hci.apps.vhoe.utils;

import android.bluetooth.BluetoothDevice;

import com.hci.apps.vhoe.models.Message;

import java.util.List;

/**
 * Created by abdelrahman on 21.05.17.
 */

public interface BluetoothListener {
    void handleMessage(Message message);
    void deviceUpdated(List<BluetoothDevice> pairedDevices, List<BluetoothDevice> discoveredDevices);
    void onDeviceConnect(BluetoothDevice device);
}

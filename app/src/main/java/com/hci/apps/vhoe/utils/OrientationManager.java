package com.hci.apps.vhoe.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by abdelrahman on 01.07.17.
 */

public class OrientationManager implements SensorEventListener{
    private static OrientationManager instance ;

    public static OrientationManager getInstance(Context context){
        if(instance == null)
            instance = new OrientationManager(context);
        return instance;
    }

    private OrientationManager(Context context){
        init(context);
    }

    private SensorManager mSensorManager;
    private Sensor gsensor;
    private Sensor msensor;
    float[] mGravity;
    float[] mGeomagnetic;

    public float getAzimut() {
        return (float) Math.toDegrees(azimut.doubleValue());
    }

    private static Float azimut;  // View to draw a compas

    public void init(Context context){
        mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        gsensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        msensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSensorManager.registerListener(this, gsensor, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, msensor, SensorManager.SENSOR_DELAY_GAME);

    }
    public void stop(){
        mSensorManager.unregisterListener(this);
        instance =null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimut = orientation[0]; // orientation contains: azimut, pitch and roll
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}

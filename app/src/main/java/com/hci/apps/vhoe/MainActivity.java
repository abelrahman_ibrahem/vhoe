package com.hci.apps.vhoe;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hci.apps.vhoe.utils.BluetoothManager;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    GoogleMap map;
    public static Location myPosition;
    GoogleApiClient mGoogleApiClient;
    public static final int LOCATION_REQUEST_CODE = 10;
    public static final int BLUETOOTH_REQUEST_CODE = 11;
    public static final int BLUETOOTH_ADMIN_REQUEST_CODE = 12;
    public static BluetoothManager bluetoothManager;
    public final String START_SERVICE = "Start Vibrotactile Notifications";
    public final String STOP_SERVICE = "Stop Vibrotactile Notifications";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bluetoothManager= BluetoothManager.getInstance(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng sydney = new LatLng(-34, 151);
                MarkerOptions sydneyMarkr = new MarkerOptions().position(sydney).title("Marker in Sydney");
                Marker mrkr = map.addMarker(sydneyMarkr);
                mrkr.setRotation(135);
                map.animateCamera(CameraUpdateFactory.newLatLng(sydney));
            }
        });


        final FloatingActionButton serviceButton = (FloatingActionButton) findViewById(R.id.serviceButton);

        boolean alarmUp = (PendingIntent.getService(getApplicationContext(), 0,
                new Intent("com.hci.apps.vhoe.LocationService"),
                PendingIntent.FLAG_NO_CREATE) != null);
        if(alarmUp){
            serviceButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_stop_white_24dp));
        }else{
            serviceButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_tap_and_play_white_24dp));
        }
        serviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean alarmUp = (PendingIntent.getService(getApplicationContext(), 0,
                        new Intent(getApplicationContext(), LocationService.class),
                        PendingIntent.FLAG_NO_CREATE) != null);
                AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
                Log.d(TAG,"Alrm up is "+alarmUp +"locationservice running "+ isLocationServiceRunning(LocationService.class));
                if(alarmUp){
                    alarmMgr.cancel( PendingIntent.getService(getApplicationContext(), 0, new Intent(getApplicationContext(), LocationService.class), 0));
                    serviceButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_tap_and_play_white_24dp));

                }else if (isLocationServiceRunning(LocationService.class)){
                    getApplicationContext().stopService(new Intent(getApplicationContext(),LocationService.class));
                    serviceButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_tap_and_play_white_24dp));

                }else {
                    getApplicationContext().startService(new Intent(getApplicationContext() , LocationService.class));
                    serviceButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_stop_white_24dp));

                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }




    }
    private boolean isLocationServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        if(bluetoothManager!= null)
            bluetoothManager.startService();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        // Getting LocationManager object from System Service LOCATION_SERVICE
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST_CODE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        map.setMyLocationEnabled(true);
        if(map.getMyLocation()!= null){
            float zoomLevel = 16.0f; //This goes up to 21
            Log.d("Location","zoomin in ");
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(map.getMyLocation().getLatitude(),map.getMyLocation().getLongitude()), zoomLevel));
        }
//        CameraUpdate update = CameraUpdateFactory.newLatLng(new LatLng(map.getMyLocation().getLatitude(),map.getMyLocation().getLongitude()));
//        map.
        // Create an instance of GoogleAPIClient.

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                map.setMyLocationEnabled(true);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {

//        } else if (id == R.id.nav_share) {
        if( id == R.id.nav_bluettoth) {
            startActivity(new Intent(this, BluetoothActivity.class ));
        }
        if( id == R.id.nav_location) {
            startActivity(new Intent(this, ReferenceLocationActivity.class ));
        }

        if( id == R.id.nav_share){

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        myPosition = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (myPosition != null) {
            Log.d("Location", "Last location received" + myPosition.toString());
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(myPosition.getLatitude(), myPosition.getLongitude()),16.0f));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}

int LedPin = 8;
int LedPin1 = 9;
int LedPin2 = 10;
int LedPin3 = 11;
int LedPin4 = 12;
int LedPin5 = 13;
int level;
char cmd;
void setup()
{
  // define the PIN as OUTPUT 
  pinMode(LedPin,OUTPUT); 
  pinMode(LedPin1,OUTPUT); 
  pinMode(LedPin2,OUTPUT); 
  pinMode(LedPin3,OUTPUT); 
  pinMode(LedPin4,OUTPUT); 
  pinMode(LedPin5,OUTPUT); 
   
  
  // set baud rate 9600
  Serial.begin(9600);
}

void loop()
{ 
  while (Serial.available()) //connected
  {
    cmd = Serial.read();
    Serial.println(char(cmd), DEC);
    Serial.println(cmd);
    if(cmd-int('A') < 10)
    {
      // direction 6
      // level
      Serial.print("Directon 6: ");
      Serial.println(LedPin5);
      level = cmd-'A'+1;
      Serial.println(level);
      digitalWrite(LedPin5,HIGH);
      delay(level*1000);
      digitalWrite(LedPin5,LOW);
    }
    else if(cmd-int('K') < 10)
    {
      // direction 5
      // level
      Serial.print("Directon 5: ");
      
      level = cmd-'K'+1;
      Serial.println(LedPin4);
      Serial.println(level);
      digitalWrite(LedPin4,HIGH);
      delay(level*1000);
      digitalWrite(LedPin4,LOW);
    }
    else if(cmd-int('U') < 10)
    {
      // direction 4
      // level
      level = cmd-'U'+1;
      Serial.print("Directon 4: ");
      Serial.println(LedPin3);
      Serial.println(level);
      digitalWrite(LedPin3,HIGH);
      delay(level*1000);
      digitalWrite(LedPin3,LOW);
    }
    else if(cmd-int('_') < 10)
    {
      //direction 3
      // level
      Serial.print("Directon 3: ");
      Serial.println(LedPin2);
      level = cmd-'_'+1;
      Serial.println(level);
      digitalWrite(LedPin2,HIGH);
      delay(level*1000);
      digitalWrite(LedPin2,LOW);
    }
    else if(cmd-int('i') < 10)
    {
      //direction 2
      // level
      Serial.print("Directon 2: ");
      Serial.println(LedPin1);
      level = cmd-'i'+1;
      Serial.println(level);
      digitalWrite(LedPin1,HIGH);
      delay(level*1000);
      digitalWrite(LedPin1,LOW);
    }
    else if(cmd-int('s') < 10)
    {
      // direction 1
      // level
      Serial.print("Directon 1: ");
      Serial.println(LedPin);
      level = cmd-'s'+1;
      Serial.println(level);
      digitalWrite(LedPin,HIGH);
      delay(level*1000);
      digitalWrite(LedPin,LOW);
    }else{
      Serial.println("Else ");
    }
   /* if(cmd == 'a') //press 'a' to turn on LED
    {
      digitalWrite(LedPin,HIGH);
    }
    else if(cmd == 'g') //press 'g' to turn off LED
    {
      
      digitalWrite(LedPin,LOW);
    } */   
  }
}

